FROM centos:7
LABEL MAINTAINER="Tobias Florek <tob@butter.sh>"

EXPOSE 25/tcp

RUN set -x \
 && rpmkeys --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 \
 && yum --setopt=tsflags=nodocs -y install epel-release \
 && rpmkeys --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 \
 && yum --setopt=tsflags=nodocs -y install opensmtpd gettext \
 && yum clean all \
 && mkdir /usr/libexec/opensmtpd-container

ADD smtpd.conf.template /etc/opensmtpd/smtpd.conf.template
ADD entrypoint.sh /usr/libexec/opensmtpd-container/entrypoint.sh

# log only config messages
ENTRYPOINT ["/usr/libexec/opensmtpd-container/entrypoint.sh"]
CMD ["/usr/sbin/smtpd", "-d"]

VOLUME ["/var/spool", "/var/run/"]
