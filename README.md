# Opensmtpd-Container that forwards mail to a remote server

It will listen on localhost:25 and accept any mail, forwarding to the configured smtp server using authentication and TLS (unless overwritten).

It uses the following environment variables.

 * `SMTP_USER`: the remote smtp user,
 * `SMTP_PASS`: the remote smtp password,
 * `SMTP_SERVER_URI`: the remote smtp server, e.g. "smtp+tls://my_user@smtp.example.com:587",
 * `SMTP_SERVER`: the remote server if `SMTP_SERVER_URI` is not specified,
 * `SMTP_PORT`: the remote server's port if `SMTP_SERVER_URI` is not specified, default 587.
