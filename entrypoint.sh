#!/bin/bash
set -euo pipefail

# export all variables
set -a

: "${SMTPD_CONF_TEMPLATE:=/etc/opensmtpd/smtpd.conf.template}"
: "${SMTP_SECRETS_FILE:=/var/secrets/opensmtpd/smtp-secrets}"
: "${SMTP_PORT:=587}"

err() {
    printf "%s\n" "$*" 1>&2
}

if [ -n "$SMTP_SERVER" ] && [ -n "$SMTP_USER" ]; then
    : "${SMTP_SERVER_URI:=tls+auth://forwarder@${SMTP_SERVER}:${SMTP_PORT}}"
fi

# do not overwrite STMP_SECRETS_FILE if non-empty
if ! [ -s "${SMTP_SECRETS_FILE}" ] \
    && [ -n "$SMTP_USER" ] \
    && [ -n "$SMTP_PASS" ]; then
        mkdir -p "$(dirname "$SMTP_SECRETS_FILE")"
        printf "%s %s:%s\n" "forwarder" "$SMTP_USER" "$SMTP_PASS" \
            > "$SMTP_SECRETS_FILE"
fi

if [ -z "$SMTP_SERVER_URI" ]; then
    err "Not all variables set!"
    err "Set either SMTP_SERVER_URI or SMTP_USER, SMTP_SERVER and SMTP_PORT."
    exit 1
fi

echo generate_config
envsubst < "$SMTPD_CONF_TEMPLATE" > /etc/opensmtpd/smtpd.conf

exec "$@"
